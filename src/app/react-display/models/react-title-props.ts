import { NgRedux } from '@angular-redux/store'
import { AppState } from '../../store/state/app.state'

export interface ReactTitleProps {
  title: string
  subtitle: string
  onTitleChange: ( title: string ) => void
  onSubtitleChange: ( subtitle: string ) => void
}
