import React from 'react'

const Subtitle = ({ subtitle }) => (
  <p>{subtitle}</p>
);

export default Subtitle;
