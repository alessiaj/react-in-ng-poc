import React from 'react';
import { connect } from 'react-redux'
import Display from '../components/Display';
import { selectDisplaySubtitle, selectDisplayTitle } from '../store/selectors/display.selector';
import { dSetSubtitle, dSetTitle } from '../store/actions/display.action';

const mapStateToProps = state => ({
  title: selectDisplayTitle( state ),
  subtitle: selectDisplaySubtitle( state )
});

const mapDispatchToProps = dispatch => ({
  onTitleChange: title => dispatch( dSetTitle( title )),
  onSubtitleChange: subtitle => dispatch( dSetSubtitle( subtitle ) )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Display)
