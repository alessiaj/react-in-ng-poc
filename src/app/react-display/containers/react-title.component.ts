import { NgRedux, select } from '@angular-redux/store'
import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core'
import { createElement } from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { Observable } from 'rxjs'
import { AppState } from '../../store/state/app.state'
import { dSetSubtitle, dSetTitle } from '../../display/store/actions/display.action'
import { selectSubtitle, selectTitle } from '../../display/store/selectors/display.selector'
import ReactTitle from './ReactTitle'

@Component( {
  selector: 'app-react-title',
  template: `
    <div #react></div>`,
  styles: []
} )
export class ReactTitleComponent implements AfterViewInit {

  @select( selectTitle ) title$: Observable<string>
  @select( selectSubtitle ) subtitle$: Observable<string>
  @ViewChild( 'react' ) protected container: ElementRef

  constructor( private ngRedux: NgRedux<AppState> ) {
  }

  onTitleChange( value: string ) {
    this.ngRedux.dispatch( dSetTitle( value ) )
  }

  onSubtitleChange( value: string ) {
    this.ngRedux.dispatch( dSetSubtitle( value ) )
  }

  ngAfterViewInit() {
    render(
      createElement( Provider, { store: this.ngRedux as any }, createElement( ReactTitle ) ),
      this.container.nativeElement
    )
  }
}

