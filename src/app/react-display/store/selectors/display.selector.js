import { createSelector } from 'reselect';

export const selectDisplayState = state => state.display;

export const selectDisplayTitle = createSelector(
  selectDisplayState,
  state => state.title
);

export const selectDisplaySubtitle = createSelector(
  selectDisplayState,
  state => state.subtitle
);
