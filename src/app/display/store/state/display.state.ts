export interface DisplayState {
  title?: string
  subtitle?: string
}
