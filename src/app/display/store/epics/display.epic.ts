import { Injectable } from '@angular/core'
import { FSA } from 'flux-standard-action'
import { EMPTY, Observable } from 'rxjs'
import { mergeMap, tap } from 'rxjs/operators'
import { DisplayActionTypes } from '../actions/display.action'

@Injectable({providedIn: 'root'})
export class DisplayEpic {

  getEpics = () => [ this.setTitle, this.setSubtitle ]

  setTitle = ( action$: Observable<FSA<DisplayActionTypes, string>> ) =>
    action$.pipe(
      tap( ({payload}) => console.log(`This is the new title: ${payload}`)),
      mergeMap( () => EMPTY )
    )

  setSubtitle = ( action$: Observable<FSA<DisplayActionTypes, string>> ) =>
    action$.pipe(
      tap( ({payload}) => console.log(`This is the new subtitle: ${payload}`)),
      mergeMap( () => EMPTY )
    )
}
