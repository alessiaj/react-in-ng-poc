import { FSA } from 'flux-standard-action'

export enum DisplayActionTypes {
  SET_TITLE = '[DISPLAY] Set Title',
  SET_SUBTITLE = '[DISPLAY] Set Subtitle'
}

export const dSetTitle = ( title: string ): FSA<DisplayActionTypes, string> => ({
  type: DisplayActionTypes.SET_TITLE,
  payload: title
})

export const dSetSubtitle = ( subtitle: string ): FSA<DisplayActionTypes, string> => ({
  type: DisplayActionTypes.SET_SUBTITLE,
  payload: subtitle
})
