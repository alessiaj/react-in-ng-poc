import { createSelector, Selector } from 'reselect'
import { selectDisplayState } from '../../../store/selectors/app.selector'
import { AppState } from '../../../store/state/app.state'
import { DisplayState } from '../state/display.state'

export const selectTitle: Selector<AppState, string> = createSelector<AppState,
  DisplayState,
  string>(
  selectDisplayState,
  ( state ) => state.title
)

export const selectSubtitle: Selector<AppState, string> = createSelector<AppState,
  DisplayState,
  string>(
  selectDisplayState,
  state => state.subtitle
)
