import { FSA } from 'flux-standard-action'
import { DisplayActionTypes } from '../actions/display.action'
import { DisplayState } from '../state/display.state'

const INITIAL_STATE: DisplayState = {
  title: 'POC App',
  subtitle: 'This is a POC App'
}

export function displayReducer(
  state: DisplayState = INITIAL_STATE,
  action: FSA<DisplayActionTypes, string>
): DisplayState {
  switch ( action.type ) {
    case DisplayActionTypes.SET_TITLE:
      return {
        ...state,
        title: action.payload
      }
    case DisplayActionTypes.SET_SUBTITLE:
      return {
        ...state,
        subtitle: action.payload
      }
    default:
      return state
  }
}
