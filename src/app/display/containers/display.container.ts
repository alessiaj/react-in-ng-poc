import { NgRedux, select } from '@angular-redux/store'
import { Component } from '@angular/core'
import { Observable } from 'rxjs'
import { AppState } from '../../store/state/app.state'
import { dSetSubtitle, dSetTitle } from '../store/actions/display.action'
import { selectSubtitle, selectTitle } from '../store/selectors/display.selector'

@Component( {
  selector: 'app-display',
  template: `
    <app-title [title]="title$ | async"></app-title>
    <app-subtitle [subtitle]="subtitle$ | async"></app-subtitle>
    <app-input type="title" (valueChange)="onTitleChange($event)"></app-input>
    <app-input type="subtitle" (valueChange)="onSubtitleChange($event)"></app-input>
  `,
  styles: []
} )
export class DisplayContainer {

  @select( selectTitle ) title$: Observable<string>

  @select( selectSubtitle ) subtitle$: Observable<string>

  constructor( private ngRedux: NgRedux<AppState> ) {
  }

  onTitleChange( value: string ) {
    this.ngRedux.dispatch( dSetTitle( value ) )
  }

  onSubtitleChange( value: string ) {
    this.ngRedux.dispatch( dSetSubtitle( value ) )
  }
}
