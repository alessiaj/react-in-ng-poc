import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-subtitle',
  template: `<p>{{subtitle}}</p>`,
  styles: []
})
export class SubtitleComponent {
  @Input() subtitle: string
}
