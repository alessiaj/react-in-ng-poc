import { Component, EventEmitter, Input, Output } from '@angular/core'

@Component({
  selector: 'app-input',
  template: `
    <div>
      <label for="s-input">Enter {{type}}: </label>
      <input #inputElement name="s-input" (keyup)="onKey(inputElement.value)">
    </div>
  `
})
export class InputComponent {

  @Input() type: string

  @Output() valueChange: EventEmitter<string>

  constructor() {
    this.type = 'value'
    this.valueChange = new EventEmitter<string>()
  }

  onKey( value: string ) {
    this.valueChange.emit( value )
  }
}
