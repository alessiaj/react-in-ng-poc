import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DisplayContainer } from './display/containers/display.container'
import { NgReactTitleComponent } from './ng-react/containers/ng-react-title.component'
import { ReactTitleComponent } from './react-display/containers/react-title.component'

const routes: Routes = [
    {
      path: 'ng',
      component: DisplayContainer
    },
    {
      path: 'react-display',
      component: ReactTitleComponent
    },
    {
      path: 'ms-ng-react-display',
      component: NgReactTitleComponent
    }
    ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
