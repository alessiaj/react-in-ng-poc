import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <p><a routerLink="/ng">Ng component</a></p>
    <p><a routerLink="/react-display">Manual React</a></p>
    <p><a routerLink="/ms-ng-react-display">MS Angular React</a></p>
   <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
}
