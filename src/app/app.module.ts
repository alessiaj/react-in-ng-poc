import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InputComponent } from './display/components/input.component'
import { SubtitleComponent } from './display/components/subtitle.component'
import { TitleComponent } from './display/components/title.component'
import { DisplayContainer } from './display/containers/display.container'
import { NgReactTitleComponent } from './ng-react/containers/ng-react-title.component'
import { ReactTitleComponent } from './react-display/containers/react-title.component'
import { StoreModule } from './store/store.module'

@NgModule({
  declarations: [
    AppComponent,
    DisplayContainer,
    TitleComponent,
    SubtitleComponent,
    InputComponent,
    ReactTitleComponent,
    NgReactTitleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule { }
