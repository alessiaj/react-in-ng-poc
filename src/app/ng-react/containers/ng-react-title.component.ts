import { ReactWrapperComponent } from '@angular-react/core'
import { NgRedux, select } from '@angular-redux/store'
import { ChangeDetectorRef, Component, ElementRef, NgZone, Renderer2, ViewChild } from '@angular/core'
import { Observable } from 'rxjs'
import { dSetSubtitle, dSetTitle } from '../../display/store/actions/display.action'
import { selectSubtitle, selectTitle } from '../../display/store/selectors/display.selector'
import { ReactTitleProps } from '../../react-display/models/react-title-props'
import { AppState } from '../../store/state/app.state'

@Component( {
  selector: 'app-ng-react-title',
  template: `
    <Display
        #reactNode
        [title]="title$ | async"
        [subtitle]="subtitle$ | async"
        [TitleChange]="onTitleChange"
        [SubtitleChange]="onSubtitleChange"
    ></Display>
  `,
  styles: []
} )
export class NgReactTitleComponent extends ReactWrapperComponent<ReactTitleProps> {
  @ViewChild( 'reactNode' ) protected reactNodeRef: ElementRef<HTMLElement>

  @select( selectTitle ) title$: Observable<string>
  @select( selectSubtitle ) subtitle$: Observable<string>

  constructor( elementRef: ElementRef, changeDetectorRef: ChangeDetectorRef, renderer: Renderer2, ngZone: NgZone, protected ngRedux: NgRedux<AppState> ) {
    super( elementRef, changeDetectorRef, renderer, { setHostDisplay: true, ngZone } )
    this.onTitleChange.bind(this)
    this.onSubtitleChange.bind(this)
  }

  onTitleChange( value: string ) {
    this.ngRedux.dispatch( dSetTitle( value ) )
  }

  onSubtitleChange( value: string ) {
    this.ngRedux.dispatch( dSetSubtitle( value ) )
  }
}

