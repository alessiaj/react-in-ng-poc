export const DISPLAY_ACTION_TYPES = {
  SET_TITLE: '[DISPLAY] Set Title',
  SET_SUBTITLE: '[DISPLAY] Set Subtitle'
};

export const dSetTitle = ( title ) => ({
  type: DISPLAY_ACTION_TYPES.SET_TITLE,
  payload: title
});

export const dSetSubtitle = ( subtitle ) => ({
  type: DISPLAY_ACTION_TYPES.SET_SUBTITLE,
  payload: subtitle
});
