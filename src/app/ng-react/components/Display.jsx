import React from 'react'
import Title from './Title';
import Subtitle from './Subtitle';
import Input from './Input';

const Display = ({ title, subtitle, onTitleChange, onSubtitleChange }) => (
  <div>
    <Title title={title} />
    <Subtitle subtitle={subtitle} />
    <Input type="title" onKeyHandler={onTitleChange} />
    <Input type="subtitle" onKeyHandler={onSubtitleChange} />
  </div>
);

export default Display;
