import React from 'react';

const Input = ({ type, onKeyHandler }) => (
  <div>
    <label htmlFor="s-input">Enter {type}: </label>
    <input name="s-input" onKeyUp={(e) => onKeyHandler(e.target.value)} />
  </div>
);

export default Input;
