import { DevToolsExtension, NgRedux, NgReduxModule } from '@angular-redux/store'
import { NgModule } from '@angular/core'
import { createEpicMiddleware } from 'redux-observable'
import { AppEpic } from './epics/app.epic'
import { appReducer } from './reducers/app.reducer'
import { AppState } from './state/app.state'
import { createLogger } from 'redux-logger'

@NgModule({
  imports: [ NgReduxModule ],
  exports: [ NgReduxModule ]
})
export class StoreModule {
  constructor (
    public store: NgRedux<AppState>,
    public devTools: DevToolsExtension,
    public appEpic: AppEpic
  ) {
    const epicMiddleware = createEpicMiddleware()
    const middleware = [ epicMiddleware, createLogger({ collapsed: true }) ]
    store.configureStore( appReducer, {}, middleware, [devTools.enhancer()] )

    epicMiddleware.run( this.appEpic.getEpicList() )
  }
}
