import { DisplayState } from '../../display/store/state/display.state'

export interface AppState {
  display?: DisplayState
}
