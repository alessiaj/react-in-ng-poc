import { Selector } from 'reselect'
import { DisplayState } from '../../display/store/state/display.state'
import { AppState } from '../state/app.state'

export const selectDisplayState: Selector<AppState, DisplayState> = state => state.display
