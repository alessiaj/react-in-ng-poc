import { combineReducers } from 'redux'
import { displayReducer } from '../../display/store/reducers/display.reducer'
import { AppState } from '../state/app.state'

export const appReducer = combineReducers<AppState>({
  display: displayReducer
})
