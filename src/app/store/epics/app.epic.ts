import { flatten } from 'lodash'
import { Injectable } from '@angular/core'
import { combineEpics } from 'redux-observable'
import { DisplayEpic } from '../../display/store/epics/display.epic'

@Injectable({providedIn: 'root'})
export class AppEpic {
  constructor( private displayEpic: DisplayEpic ) {}

  getEpicList = () => combineEpics(
    ...flatten([
      this.displayEpic.getEpics()
    ])
  )
}
